import {Component, OnInit, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {ChatService} from './chat.service';


@Component({
  selector: 'app-chat',
  templateUrl: 'chat.template.html',
  providers: [ChatService]
})
export class ChatComponent implements OnInit, OnDestroy {

  user: string;

  connection: any;
  chatMessages: Array<any> = [];
  clientMessage = '';

  constructor(private chatService: ChatService) {}

  ngOnInit() {
    this.connection = this.chatService.getMessages().subscribe(message => {
      this.chatMessages.push(message);
    });
  }

  ngOnDestroy() {
    this.connection.unsubscribe();
  }

  sendMessage() {
    this.chatService.sendMessage(this.user, this.clientMessage);
    this.clientMessage = '';
  }

}
