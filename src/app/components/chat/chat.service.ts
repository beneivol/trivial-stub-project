import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as io from 'socket.io-client';

@Injectable()
export class ChatService {

  private serverUrl = 'http://localhost:8080';
  private socket: any;

  getMessages(): Observable<any> {
    const observable = new Observable(observer => {
      this.socket = io(this.serverUrl);

      this.socket.on('message', (data) => {
        observer.next(data);
      });

      return () => {
        this.socket.disconnect();
      };
    });

    return observable;
  }

  sendMessage(user, message) {
    this.socket.emit('add-message', user, message);
  }

}
